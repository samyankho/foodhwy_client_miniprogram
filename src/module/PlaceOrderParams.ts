import {User} from "@/module/User";
import {Product} from "@/module/Product";
import {GlobalSetting} from "@/GlobalSetting";
import {Contact} from "@/module/Contact";

export interface PlaceOrderParams {
  shop_id: number
  shipping_fee: number
  shipping_distance: number
  contact: Contact | string
  items: string
  comment: string
  shipping_type: string
  payment_type: string
  order_type: string
  subtotal: number
  promotion_code: string
  tips_amount: number
  // discount_product_ids: number[]
  //非必要上传
  payment_fee: number;
  service_fee: number;
  tax_fee: number;
  tax_rate: number;
  cart_id: string;
  delivery_note: string
  customer_note: string
}

export class PlaceOrderParams {
  public shop_id = 0
  public shipping_fee = 0
  public shipping_distance = 0
  public contact: Contact|string = GlobalSetting.getInstance().getContact();
  public items = ""
  public comment = ""
  public shipping_type = ""
  public payment_type = ""
  public order_type = ""
  public subtotal = 0
  promotion_code = ""
  public tips_amount = 0
  // public discount_product_ids: number[] = []
  //非必要上传
  public payment_fee = 0;
  public service_fee = 0;
  public tax_fee  = 0;
  public tax_rate  = 0;
  public cart_id = "";
  public delivery_note = ""
  public customer_note = ""
}
