export interface  UserDetail {
  username: string;
  cid: number;
  name: string;
  tel: string;
  address: string;
  credit: number;
  deposit: number;
  avatar: string;
  invite_code: string;
  order_count: number;
  coupons_count: number;
  member: Member
}

export interface Member{
	member_status: number;
	info: Info;
}

export class Member{
	public member_status= 0;
	public info= new Info;
}

export interface Info{
	end_day: string;
	renew_status: number;
}

export class Info{
	public end_day= "";
	public renew_status= 0;
}

export class UserDetail {
  public username = ""
  public cid = 0
  public name = ""
  public tel = ""
  public credit= 0
  public deposit = 0
  public avatar= ""
  public invite_code= ""
  public order_count= 0
  public coupons_count= 0
  public loginAt = 0;
  public password ='';
  public phone_num=''
  public member = new Member()
}


