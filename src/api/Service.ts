export enum Service {
  SandboxServer= "https://sandbox.foodhwy.com/api",
  // FormalServer: "https://sandbox.foodhwy.com/api",
  FormalServer= "https://wechat.foodhwy.com/api",
  TestServer= "http://php7.foodhwy.net/api",
}