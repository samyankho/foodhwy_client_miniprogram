import {AddrBook, ExistedAddrBook} from "@/module/AddrBook";
import {ApiHelper} from "@/api/ApiHelper";
import {CartLogic} from "@/logic/CartLogic";
import {ShopDetail} from "@/module/ShopDetail";

export class AddressLogic {
  private static addrBookLogic: AddressLogic;
  private apiHelper = ApiHelper.getInstance();
  private cartLogic: CartLogic = CartLogic.getInstance();
  private shopDetail: ShopDetail = this.cartLogic.getShopDetail()
  private addrBookList: ExistedAddrBook[] = [];
  private enableSelect = 1;
  private addrLatLng: string = ""
  private searchResult = []

  public static getInstance() {
    if (!this.addrBookLogic) {
      this.addrBookLogic = new AddressLogic();
    }
    return this.addrBookLogic;
  }

  public async fetchAddrBook() {
    try {
      const response: any = await this.apiHelper.getAddressBook();
      this.addrBookList = response;
      return response
    } catch (e) {
      console.log(e);
    }
  }


  public getAddrBook(): ExistedAddrBook[] {
    return this.addrBookList;
  }

  public async createAddrBook(addrBook: AddrBook) {
    try {
      const response = await this.apiHelper.postNewAddressBook(addrBook);
    } catch (e) {
      console.log(e);
    }
  }

  public async updateAddrBook(addrBook: ExistedAddrBook) {
    try {
      const response = await this.apiHelper.postUpdateAddressBook(addrBook);
    } catch (e) {
      console.log(e);
    }
  }

  public async deleteAddrBook(id: number) {
    try {
      const response = await this.apiHelper.postDeleteAddressBook(id);
    } catch (e) {
      console.log(e);
    }
  }
  
  public async searchAddress(addr: string){
	try {
		const response: any = await this.apiHelper.getSearchAddress(addr);
		this.searchResult = response.predictions
	} catch (e) {
		console.log(e);
	}
  }
  
  public async fetchAddressLatLng(addr: string){
  	try {
  		const response: any = await this.apiHelper.fetchAddressLatLng(addr);
  		this.addrLatLng = response
  	} catch (e) {
		console.log(e);
  	}
  }
  
  public async isOutOfRange(addr: string){
	  try {
	  	const response: any = await this.apiHelper.isOutOfRange(addr, this.shopDetail.lat + ',' + this.shopDetail.lon, this.shopDetail.delivery_setting_ids);
		return response;
	  } catch (e) {
	  	console.log(e);
	  }
  }

  // public checkResState(res: any): boolean {
  //   if (res.data.status === 0) {
  //     alert(res.data.error);
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }
  
  public getAddrLatLng(){
	  return this.addrLatLng
  }
  
  public getSearchResult(){
	  return this.searchResult
  }

  public getEnableSelect(): boolean{
    return this.enableSelect === 1;
  }

  public setEnableSelect(enable: number){
    this.enableSelect = enable
  }


}
