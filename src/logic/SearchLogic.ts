import {ApiHelper} from "@/api/ApiHelper";
import {GlobalSetting} from "@/GlobalSetting";
import {OrderHistory} from "@/module/OrderHistory";
import {Shop} from "@/module/Shop";

export class SearchLogic {

	private apiHelper = ApiHelper.getInstance();
	private globalSetting: GlobalSetting = GlobalSetting.getInstance();
	
	private static searchLogic: SearchLogic;
	private order_history: OrderHistory[] = [];
	private shopList:Shop[] = [];


	public static getInstance() {
		if (!this.searchLogic) {
		this.searchLogic = new SearchLogic();
		}
		return this.searchLogic;
	}
	
	public isLogin(){
		return this.globalSetting.isLogin()
	}
	
	public async fetchOrderHistory(){
		const response: any = await this.apiHelper.fetchOrderList();
		let historyList = response.finished;
		historyList.forEach((e:any) => {
			this.duplicateStore(e.shop_id) ? '' : this.order_history.push(e)
		})
	}
	
	public duplicateStore(mid: number){
		let result = false
		this.order_history.forEach((e:any) => {
			e.shop_id===mid ? result = true : ""
		})
		return result;
	}
	
	public getFinishedOrderList() {
	    return this.order_history;
	}
	
	public async search(city_id:number, search:string){
		const response: any = await this.apiHelper.getSearchedShopList(city_id, search)
		this.shopList = response.list
		return this.shopList
	}
	
}