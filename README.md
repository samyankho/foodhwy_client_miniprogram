### 项目使用了uniapp框架进行开发		[uniapp介绍](https://uniapp.dcloud.io/)
### uniapp推荐使用hbuilderx开发工具	[hbuilderx下载](https://www.dcloud.io/hbuilderx.html)
### 开发微信小程序需要下载微信小程序开发工具		[微信小程序开发工具下载](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)

# 项目代码拉取和本地开发的主要步骤
### 1. develop 分支为开发分支，master分支为线上运行分支，提交或者拉取代码在develop分支进行。

### 2. 切换环境在GlobalSetting的getEnvironment方法中切换



# 向微信公众平台上传代码的步骤


### **使用hbuilderx**
#### 1. Alt+R 选择运行到微信开发者工具	（首次运行需要先打开微信开发者工具设置里开启代理设置，如果是路径问题Alt+T设置里检查运行配置的路径）
#### 2. 微信开发者工具右上角点击上传


### **使用其他开发工具**
#### 1. npm run dev:mp-weixin 运行项目，然后用微信开发者工具打开项目里dist文件夹中dev文件夹的mp-weixin
#### 2. 微信开发者工具右上角点击上传



# 代码结构简要归纳

### 1. 所有项目代码文件位于 src 文件夹内部

### 2. 因为小程序单个包不能超过2M，所以使用了分包加载。pages是主包，pagesA是分包，所有tabbar页面必须放在主包。

### 3. 主包图片资源位于 /src/static/image 文件夹中，分包位于 /src/pagesA/static/image

### 4. 所有api位于 /src/api 文件夹

### 5. 枚举及各种数据类型位于 /src/module 文件夹

### 6. UI视图层(各路由页面)位于 /src/pages 和/src/pagesA 文件件中

### 7. 主包UI视图组件位于 /src/components 文件夹中， 分包位于 /src/pagesA/components

### 8. 逻辑层文件位于 /src/logic 文件夹中

### 9. 路由以及tabbar在 /src/pages.json 文件中进行配置

### 10. 由于项目使用ts，因此所有接口接收的内容必须先转化为对象（或interface）进行处理，统一放置在 /src/module 以实例化供调用

###  本项目分层为api层 -> 数据层 -> 逻辑层 -> 视图层
     1. api层主要由apihelper.ts和 api.ts文件构成，主要负责调取接口
     2. 数据层存放各种数据类型以及枚举，请把数据定义为合适的interface或者class放在module文件夹内来对接api
     3. 逻辑层统一安置于 /src/logic 主要放置业务逻辑，和以业务逻辑相关的数据处理
     4. 视图view层主要进行UI交互处理不进行复杂的业务处理判断，直接与逻辑层进行对接
     5. 视图compoment层级只针对UI层进行交互处理，不建议直接调用逻辑层